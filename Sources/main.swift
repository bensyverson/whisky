import Foundation

// If you need the headers, method or path, you can use this struct—otherwise you can use your own Codable struct
struct OpenWhiskRequest: Codable {
	// Your properties go here:
    let name: String?
	
	// Built-in OpenWhisk properties:
    let headers: [String: String]
	private let rawMethod: String
	let path: String
	
	enum OWHTTPMethod: String, Codable {
		case GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH
	}
	
	lazy var method = OWHTTPMethod(rawValue: rawMethod)
	
    enum CodingKeys: String, CodingKey {
    	case name
    	case headers = "__ow_headers"
		case rawMethod = "__ow_method"
		case path = "__ow_path"
    }
}

// This struct conform's to OpenWhisk's HTTP response type. It allows us to control the statusCode returned, which can be helpful.
struct OpenWhiskResponse: Codable {
	let statusCode: Int
	let body: Output
	let headers: [String: String]
}

// Rework this to include whatever you want to respond with.
struct Output: Codable {
	let greeting: String
	let request: OpenWhiskRequest
}

func main(input: OpenWhiskRequest, completion: @escaping (OpenWhiskResponse?, Error?) -> Void) -> Void {
	let result = Output(greeting: "Hello", request: input)
	let response = OpenWhiskResponse(
		statusCode: 200,
		body: result,
		headers: [
			"Content-Type": "application/json"
		])
	
	completion(response, nil)
}

