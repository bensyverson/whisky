// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "Action",
    products: [
    	.executable(
            name: "Action",
            targets:  ["Action"]
        )
    ],
    dependencies: [
        // e.g.
        // .package(url: "https://github.com/vapor/jwt.git", from: "3.0.0"),
    ],
    targets: [
        .target(
            name: "Action",
            dependencies: [], // e.g. ["JWT"]
            path: "."
        )
    ]
)
