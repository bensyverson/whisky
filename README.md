# Serverless Swift Template

## Setup

### Set up Serverless & OpenWhisk

Follow the [Serverless](https://serverless.com/) framework [OpenWhisk Quick Start](https://serverless.com/framework/docs/providers/openwhisk/guide/quick-start/)

### Install Docker image

Currently using [`openwhisk/action-swift-v4.2`](https://hub.docker.com/r/openwhisk/action-swift-v4.2).

```
$ docker pull openwhisk/action-swift-v4.2
```

### Install dependencies

```
$ npm install
```

### Ensure that the project is building natively

```
$ swift build
```

### Build the project for OpenWhisk and deploy it for the first time

The [`serverless.yml`](serverless.yml) file is configured to compile the Swift source into a Zip when you run `serverless deploy`:

```
$ serverless deploy
```

## Development

### Xcode

You may want to work on your project in Xcode. If so, run:

```
$ swift package generate-xcodeproj
```

By default, the generated project is ignored by git, since it can be regenerated at any time.

### Incremental updates

As you work, you can deploy incremental updates rather than the entire service using `npm run update`:

```
$ npm run build
$ npm run update
```

`npm run update` is a shortcut for `serverless deploy --function main`

Note that deploying the function will not rebuild the Swift source, so you need to run `npm run build` before deploying the update.
